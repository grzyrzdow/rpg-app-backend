<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HeroRepository")
 */
class Hero
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $race;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $proffesion;

    /**
     * @ORM\Column(type="integer")
     */
    private $age = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $eyesColor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hairColor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $star;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $birthPlace;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sex;

    /**
     * @ORM\Column(type="float")
     */
    private $weight = 0;

    /**
     * @ORM\Column(type="float")
     */
    private $height = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siblings;

    /**
     * @ORM\Column(type="integer")
     */
    private $ww = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $us = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $k = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $odp = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $zr = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $inte = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $sw = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $ogd = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $a = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $zyw = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $s = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $wt = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $sz = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $mag = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $po = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $pp = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $experiencePoints = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player")
     * @ORM\JoinColumn(nullable=false)
     */
    private $player;

    /**
     * @ORM\Column(type="string", length=511, nullable=true)
     */
    private $avatar;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRace(): ?string
    {
        return $this->race;
    }

    public function setRace(string $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getProffesion(): ?string
    {
        return $this->proffesion;
    }

    public function setProffesion(string $proffesion): self
    {
        $this->proffesion = $proffesion;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getEyesColor(): ?string
    {
        return $this->eyesColor;
    }

    public function setEyesColor(string $eyesColor): self
    {
        $this->eyesColor = $eyesColor;

        return $this;
    }

    public function getHairColor(): ?string
    {
        return $this->hairColor;
    }

    public function setHairColor(string $hairColor): self
    {
        $this->hairColor = $hairColor;

        return $this;
    }

    public function getStar(): ?string
    {
        return $this->star;
    }

    public function setStar(string $star): self
    {
        $this->star = $star;

        return $this;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setBirthPlace(string $birthPlace): self
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getSiblings(): ?string
    {
        return $this->siblings;
    }

    public function setSiblings(string $siblings): self
    {
        $this->siblings = $siblings;

        return $this;
    }

    public function getWw(): ?int
    {
        return $this->ww;
    }

    public function setWw(int $ww): self
    {
        $this->ww = $ww;

        return $this;
    }

    public function getUs(): ?int
    {
        return $this->us;
    }

    public function setUs(int $us): self
    {
        $this->us = $us;

        return $this;
    }

    public function getK(): ?int
    {
        return $this->k;
    }

    public function setK(int $k): self
    {
        $this->k = $k;

        return $this;
    }

    public function getOdp(): ?int
    {
        return $this->odp;
    }

    public function setOdp(int $odp): self
    {
        $this->odp = $odp;

        return $this;
    }

    public function getZr(): ?int
    {
        return $this->zr;
    }

    public function setZr(int $zr): self
    {
        $this->zr = $zr;

        return $this;
    }

    public function getInte(): ?int
    {
        return $this->inte;
    }

    public function setInte(int $inte): self
    {
        $this->inte = $inte;

        return $this;
    }

    public function getSw(): ?int
    {
        return $this->sw;
    }

    public function setSw(int $sw): self
    {
        $this->sw = $sw;

        return $this;
    }

    public function getOgd(): ?int
    {
        return $this->ogd;
    }

    public function setOgd(int $ogd): self
    {
        $this->ogd = $ogd;

        return $this;
    }

    public function getA(): ?int
    {
        return $this->a;
    }

    public function setA(int $a): self
    {
        $this->a = $a;

        return $this;
    }

    public function getZyw(): ?int
    {
        return $this->zyw;
    }

    public function setZyw(int $zyw): self
    {
        $this->zyw = $zyw;

        return $this;
    }

    public function getS(): ?int
    {
        return $this->s;
    }

    public function setS(int $s): self
    {
        $this->s = $s;

        return $this;
    }

    public function getWt(): ?int
    {
        return $this->wt;
    }

    public function setWt(int $wt): self
    {
        $this->wt = $wt;

        return $this;
    }

    public function getSz(): ?int
    {
        return $this->sz;
    }

    public function setSz(int $sz): self
    {
        $this->sz = $sz;

        return $this;
    }

    public function getMag(): ?string
    {
        return $this->mag;
    }

    public function setMag(string $mag): self
    {
        $this->mag = $mag;

        return $this;
    }

    public function getPo(): ?int
    {
        return $this->po;
    }

    public function setPo(int $po): self
    {
        $this->po = $po;

        return $this;
    }

    public function getPp(): ?int
    {
        return $this->pp;
    }

    public function setPp(int $pp): self
    {
        $this->pp = $pp;

        return $this;
    }

    public function getExperiencePoints(): ?int
    {
        return $this->experiencePoints;
    }

    public function setExperiencePoints(int $experiencePoints): self
    {
        $this->experiencePoints = $experiencePoints;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }
}
