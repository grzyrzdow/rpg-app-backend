<?php

namespace App\Controller;

use App\Entity\Hero;
use App\Entity\Game;
use App\Entity\Player;
use App\Repository\GameRepository;
use App\Repository\HeroRepository;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Cloudinary\Uploader;

class HeroController extends AbstractFOSRestController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $heroRepository;

    private $playerRepository;

    private $gameRepository;

    public function __construct(EntityManagerInterface $entityManager, HeroRepository $heroRepository, PlayerRepository $playerRepository, GameRepository $gameRepository)
    {
        $this->entityManager = $entityManager;
        $this->heroRepository = $heroRepository;
        $this->playerRepository = $playerRepository;
        $this->gameRepository = $gameRepository;
    }

    public function getHeroesAction()
    {
        return $this->heroRepository->findAll();
    }

    public function getHeroAction($id)
    {
        $hero = $this->heroRepository->find($id);
        $this->denyAccessUnlessGranted('edit', $hero);
        return $this->view($hero, Response::HTTP_OK);
    }

    /**
     * @RequestParam(name="name", description="hero name")
     * 
     * @RequestParam(name="race", description="hero race")
     * 
     * @RequestParam(name="proffesion", description="hero proffesion")
     * 
     * @RequestParam(name="age", description="hero age")
     * 
     * @RequestParam(name="eyesColor", description="hero eyesColor")
     * 
     * @RequestParam(name="hairColor", description="hero hairColor")
     * 
     * @RequestParam(name="star", description="hero star")
     * 
     * @RequestParam(name="birthPlace", description="hero birthPlace")
     * 
     * @RequestParam(name="sex", description="hero sex")
     * 
     * @RequestParam(name="weight", default="0", description="hero weight")
     * 
     * @RequestParam(name="height", default="0", description="hero height")
     * 
     * @RequestParam(name="siblings", default="", description="hero siblings")
     * 
     * @RequestParam(name="description",  default="", description="hero description")
     * 
     * @RequestParam(name="ww", default="0", description="hero ww")
     * 
     * @RequestParam(name="us", default="0", description="hero us")
     * 
     * @RequestParam(name="k", default="0", description="hero k")
     * 
     * @RequestParam(name="odp", default="0", description="hero odp")
     * 
     * @RequestParam(name="zr", default="0", description="hero zr")
     * 
     * @RequestParam(name="inte", default="0", description="hero inte")
     * 
     * @RequestParam(name="sw", default="0", description="hero sw")
     * 
     * @RequestParam(name="ogd", default="0", description="hero ogd")
     * 
     * @RequestParam(name="a", default="0", description="hero a")
     * 
     * @RequestParam(name="zyw", default="0", description="hero zyw")
     * 
     * @RequestParam(name="s", default="0", description="hero s")
     * 
     * @RequestParam(name="wt", default="0", description="hero wt")
     * 
     * @RequestParam(name="sz", default="0", description="hero sz")
     * 
     * @RequestParam(name="mag", default="0", description="hero mag")
     * 
     * @RequestParam(name="po", default="0", description="hero po")
     * 
     * @RequestParam(name="pp", default="0", description="hero pp")
     * 
     * @RequestParam(name="experiencePoints", default="0", description="hero experience points")
     * 
     * @FileParam(name="avatar", requirements={"maxSize"="5M"}, image=true, default="null")
     * 
     * @param ParamFetcher $paramFetcher
     */
    public function postGameHeroAction(Game $game, ParamFetcher $paramFetcher)
    {
        $name = $paramFetcher->get('name');
        $race = $paramFetcher->get('race');
        $proffesion = $paramFetcher->get('proffesion');
        $age = $paramFetcher->get('age');
        $eyesColor = $paramFetcher->get('eyesColor');
        $hairColor = $paramFetcher->get('hairColor');
        $star = $paramFetcher->get('star');
        $birthPlace = $paramFetcher->get('birthPlace');
        $sex = $paramFetcher->get('sex');
        $weight = $paramFetcher->get('weight');
        $height = $paramFetcher->get('height');
        $siblings = $paramFetcher->get('siblings');
        $ww = $paramFetcher->get('ww');
        $us = $paramFetcher->get('us');
        $k = $paramFetcher->get('k');
        $odp = $paramFetcher->get('odp');
        $zr = $paramFetcher->get('zr');
        $inte = $paramFetcher->get('inte');
        $sw = $paramFetcher->get('sw');
        $ogd = $paramFetcher->get('ogd');
        $a = $paramFetcher->get('a');
        $zyw = $paramFetcher->get('zyw');
        $s = $paramFetcher->get('s');
        $wt = $paramFetcher->get('wt');
        $sz = $paramFetcher->get('sz');
        $mag = $paramFetcher->get('mag');
        $po = $paramFetcher->get('po');
        $pp =  $paramFetcher->get('pp');
        $avatar = $paramFetcher->get('avatar');
        $experiencePoints = $paramFetcher->get('experiencePoints');

        $hero = new Hero();
        $hero->setName($name);
        $hero->setRace($race);
        $hero->setProffesion($proffesion);
        $hero->setAge($age);
        $hero->setEyesColor($eyesColor);
        $hero->setStar($star);
        $hero->setBirthPlace($birthPlace);
        $hero->setHairColor($hairColor);
        $hero->setSex($sex);
        $hero->setWeight($weight);
        $hero->setHeight($height);
        $hero->setSiblings($siblings);
        $hero->setWw($ww);
        $hero->setUs($us);
        $hero->setK($k);
        $hero->setOdp($odp);
        $hero->setZr($zr);
        $hero->setInte($inte);
        $hero->setSw($sw);
        $hero->setOgd($ogd);
        $hero->setA($a);
        $hero->setZyw($zyw);
        $hero->SetS($s);
        $hero->SetWt($wt);
        $hero->SetSz($sz);
        $hero->SetMag($mag);
        $hero->SetPo($po);
        $hero->SetPp($pp);
        $hero->setExperiencePoints($experiencePoints);

        $player = $this->playerRepository->findOneBy([
            "user" => $this->getUser(),
            "game" => $game
        ]);

        $hero->setPlayer($player);
        if ($avatar !== "null") {
            $uploadedAvatarData = Uploader::upload($avatar, [
                "folder" => "Avatars"
            ]);
            $hero->setAvatar($uploadedAvatarData['secure_url']);
        }


        $this->entityManager->persist($hero);
        $this->entityManager->flush();

        return $this->view($hero, Response::HTTP_CREATED);
    }

    public function getGameUserHeroAction(Game $game)
    {
        $user = $this->getUser();

        $player = $this->playerRepository->findBy([
            'user' => $user,
            'game' => $game
        ]);

        $hero = $this->heroRepository->findBy([
            'player' => $player,
        ]);

        return $this->view($hero, Response::HTTP_OK);
    }
}
