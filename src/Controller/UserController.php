<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\RoleRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class UserController extends AbstractFOSRestController
{
    private $userRepository;
    private $roleRepository;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->entityManager = $entityManager;
    }


    public function getUsersCurrentAction()
    {
        $user = $this->getUser();
        return $this->view([
            "user" => $user
        ], Response::HTTP_OK)->setContext((new Context())->setGroups(["public"]));
    }

    public function getUsersAction()
    {
        $users = $this->userRepository->findAll();
        return $this->view($users, Response::HTTP_OK);
    }

    public function getUserAction(User $user)
    {
        return $this->view($user, Response::HTTP_OK);
    }
}
