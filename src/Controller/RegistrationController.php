<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Validator\Constraints;


class RegistrationController extends AbstractFOSRestController
{
    /**
     * @var RoleRepository
     */
    private $roleRepositry;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(RoleRepository $roleRepositry, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $this->roleRepositry = $roleRepositry;
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @Rest\Post("/api/register", name="api_register")
     * 
     * @RequestParam(name="username", requirements=@Constraints\Length(min = 6, max = 20), allowBlank=false, description="username for user")
     * 
     * @RequestParam(name="password", requirements=@Constraints\Length(min = 6, max = 20), allowBlank=false, description="plain password")
     * 
     * @param ParamFetcher $paramFetcher
     */
    public function index(ParamFetcher $paramFetcher, JWTTokenManagerInterface $jwtManager)
    {
        $username = $paramFetcher->get("username");
        $passwordPlain = $paramFetcher->get("password");

        $user = $this->userRepository->findOneBy([
            'username' => $username
        ]);

        if ($user) {
            return $this->view([
                'message' => 'Username taken'
            ], Response::HTTP_CONFLICT);
        }

        $user = new User();
        $user->setUsername($username);
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $passwordPlain)
        );
        $user->setRole(
            $this->roleRepositry->findOneBy([
                'name' => 'user'
            ])
        );
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $this->view([
            "user" => $user,
            "token" => $jwtManager->create($user)
        ], Response::HTTP_CREATED)->setContext((new Context())->setGroups(['public']));
    }
}
