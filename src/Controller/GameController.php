<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Game;
use App\Repository\GameRepository;
use App\Repository\UserRepository;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints;

class GameController extends AbstractFOSRestController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PlayerRepository
     */
    private $playerRepository;

    /**
     * @var GameRepository
     */
    private $gameRepository;

    public function __construct(EntityManagerInterface $entityManager, PlayerRepository $playerRepository, GameRepository $gameRepository)
    {
        $this->entityManager = $entityManager;
        $this->playerRepository = $playerRepository;
        $this->gameRepository = $gameRepository;
    }


    public function getGamesAction()
    {
        $games = $this->gameRepository->findAll();
        return $this->view($games, Response::HTTP_OK);
    }

    public function getGameAction(Game $game)
    {
        return $this->view($game, Response::HTTP_OK);
    }

    /**
     * @RequestParam(name="name", allowBlank=false, requirements=@Constraints\Length(min = 1, max = 20), description="game name")
     * 
     * @RequestParam(name="description", allowBlank=false ,requirements=@Constraints\Length(min = 1, max = 100), description="game description")
     * 
     */
    public function postGameAction(ParamFetcher $paramFetcher)
    {
        $name = $paramFetcher->get('name');
        $description = $paramFetcher->get('description');

        $game = new Game();
        $game->setName($name);
        $game->setDescription($description);
        $game->setMaster($this->getUser());

        $this->entityManager->persist($game);
        $this->entityManager->flush($game);

        return $this->view([
            "game" => $game
        ], Response::HTTP_CREATED);
    }

    public function getUserGamesAction()
    {
        $user =  $this->getUser();

        $games = $this->gameRepository->findBy([
            'master' => $user
        ]);


        return $this->view([
            "games" => $games
        ], Response::HTTP_OK);
    }
}
