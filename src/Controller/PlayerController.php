<?php

namespace App\Controller;

use App\Entity\Player;
use App\Entity\User;
use App\Entity\Game;
use App\Repository\GameRepository;
use App\Repository\PlayerRepository;
use App\Repository\UserRepository;
use App\Repository\RoleRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\Routing\Annotation\Route;


class PlayerController extends AbstractFOSRestController
{
    private $userRepository;
    private $roleRepository;
    private $playerRepository;
    private $gameRepository;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, RoleRepository $roleRepository, PlayerRepository $playerRepository, GameRepository $gameRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->entityManager = $entityManager;
        $this->playerRepository = $playerRepository;
        $this->gameRepository = $gameRepository;
    }

    public function postUserGamePlayerAction(User $user, Game $game)
    {
        if ($game->getMaster() == $user) {
            return $this->view([
                "message" => "The master can't be a player"
            ], Response::HTTP_BAD_REQUEST);
        }

        $player = new Player();
        $player->setUser($user);
        $player->setGame($game);

        $this->entityManager->persist($player);
        $this->entityManager->flush();

        return $this->view($player, Response::HTTP_CREATED);
    }

    public function getUserPlayersAction()
    {
        $players = $this->playerRepository->findBy([
            'user' => $this->getUser()
        ]);

        $games = [];
        foreach ($players as $player) {
            array_push($games, $player->getGame());
        }

        return $this->view([
            "games" => $games
        ], Response::HTTP_OK);
    }

    public function getGamePlayersAction($game)
    {
        $players = $this->playerRepository->findBy([
            'game' => $game
        ]);

        $users = [];

        foreach ($players as $player) {
            array_push($users, $player->getUser());
        }

        return $this->view([
            "players" => $users
        ], Response::HTTP_OK);
    }
}
