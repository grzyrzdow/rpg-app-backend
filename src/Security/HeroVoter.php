<?php

namespace App\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use App\Entity\Hero;
use App\Entity\User;


class HeroVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::EDIT, self::VIEW])
            && $subject instanceof Hero;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        /** @var Hero $character */
        $hero = $subject;


        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($hero, $user);
                break;
            case self::VIEW:
                return $this->canView($hero, $user);
                break;
        }

        return false;
    }

    private function canView(Hero $hero, User $user)
    {
        return true;
    }

    private function canEdit(Hero $hero, User $user)
    {
        return $user === $hero->getUser() || $user === $hero->getGame()->getMaster();
    }
}
